

name             'livewire'
maintainer       'Rotimi Akintewe'
maintainer_email 'rotimi.akintewe@konga.com'
license          'All rights reserved'
description      'Installs/Configures livewire'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'apt'
depends 'apache2', '~> 3.1.0'
depends 'php', '~> 1.7.1'
depends 'git'
depends 'composer', '~> 2.1.0'
depends 'redis', '~> 3.0.4'
depends 'php-redis', '~> 0.1.1'
depends 'mysql', '~> 6.1.0'
depends 'java', '~> 1.35.0'
depends 'elasticsearch'
depends 'composer', '~> 2.1.0'
