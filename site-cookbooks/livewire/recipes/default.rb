#
# Cookbook Name:: livewire
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt::default'
include_recipe 'apache2'
# Enable Apache PHP module
include_recipe 'apache2::mod_php5'
# Enable Apache rewrite module
include_recipe 'apache2::mod_rewrite'
include_recipe 'mysql::client'
include_recipe 'mysql::server'
include_recipe 'php'
include_recipe 'php::module_mysql'
    
include_recipe 'redis'


#Install codeception
bash "install_codeception" do
	user "root"
	cwd "/tmp"
	code <<-EOH
		wget -O /usr/local/bin/codecept http://codeception.com/codecept.phar
		chmod a+rx /usr/local/bin/codecept
	EOH
end

