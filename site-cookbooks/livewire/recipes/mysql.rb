
# Livewire Recipe to manager MySQL Server and Client

mysql_service 'lw' do
  version '5.6'
  bind_address '0.0.0.0'
  port '3306'
  initial_root_password '' 		#Secret data like password should be loaded from data_bags
  action [:create, :start]
end