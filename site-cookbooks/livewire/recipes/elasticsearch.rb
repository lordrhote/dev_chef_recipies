

 include_recipe 'java'

 elasticsearch_install 'es_livewire' do
  type :tarball # type of install
  dir '/usr/local' # where to install

  owner 'vagrant' # user and group to install under
  group 'vagrant'

  tarball_url "https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.7.1.tar.gz"
  tarball_checksum "0984ae27624e57c12c33d4a559c3ebae25e74508"

  action :install # could be :remove as well
end