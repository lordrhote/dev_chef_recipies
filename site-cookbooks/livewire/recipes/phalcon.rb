#
# Cookbook Name:: livewire
# Recipe:: phalcon
#
# Copyright 2015, Konga Online Shopping Limited
#
# All rights reserved - Do Not Redistribute


#Install Phalcon
bash "install_phalcon" do
	user "root"
	cwd "/tmp"
	code <<-EOH
		#sudo apt-add-repository ppa:phalcon/stable
		sudo apt-add-repository ppa:phalcon/legacy
		sudo apt-get update
		sudo apt-get install libpcre3-dev gcc make unzip
		sudo apt-get install php5-phalcon
		### Create the extension ini file in /etc/php5/mods-available
		if [ ! -f /etc/php5/mods-available/phalcon.ini ]; then
			echo 'extension=phalcon.so' > /etc/php5/mods-available/phalcon.ini
			ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/cli/conf.d/phalcon.ini
			#ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/apache/conf.d/phalcon.ini
			#ln -s /etc/php5/mods-available/phalcon.ini /etc/php5/fpm/conf.d/phalcon.ini
		fi
	EOH
end

#Install Phalcon Dev Tools
bash "install_phalcon_dev_tools" do
user "root"
cwd "/tmp"
code <<-EOH
		echo '{"require": {"phalcon/devtools": "dev-master"}}' > composer.json
		composer install
		rm composer.json
		if [ ! -d /opt/phalcon-tools ]; then
		  	sudo mkdir /opt/phalcon-tools
			sudo mv ~/vendor/phalcon/devtools/* /opt/phalcon-tools
			sudo ln -s /opt/phalcon-tools/phalcon.php /usr/bin/phalcon
			sudo rm -rf ~/vendor
		fi
	EOH
end

service 'apache2' do
  supports :status => true, :restart => true, :reload => true
  action [:start, :enable]
end
