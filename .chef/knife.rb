log_level                :info
log_location             STDOUT

cookbook_path    ["cookbooks", "site-cookbooks"]
node_path        "nodes"
role_path        "roles"
environment_path "environments"
data_bag_path    "data_bags"
#encrypted_data_bag_secret "data_bag_key"

#knife[:berkshelf_path] = "cookbooks"

#node_name                'vagrant'
#client_key               '/Users/RotelandO/kitchen/livewire-knife/.chef/vagrant.pem'
#validation_client_name   'chef-validator'
#validation_key           '/etc/chef-server/chef-validator.pem'
#chef_server_url          'https://MacBook-Pro.local:443'
#syntax_check_cache_path  '/Users/RotelandO/kitchen/livewire-knife/.chef/syntax_check_cache'
